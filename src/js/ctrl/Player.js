/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

require("./Compatibility");
let $ = require("jquery");
let L = require("leaflet");

let Cell = require("../model/Cell");
let DataManager = require("./service/DataManager");
let LoadingView = require("../view/Loading");
let MessagesView = require("../view/Messages");
let PlayerView = require("../view/Player");
let URLService = require("./service/URL");

/**
 * Player controller handles pictures retrieval and display
 */
let Player = {
	init: function(divId) {
		//Init DOM
		this.dom = $("#"+divId);
		this.dom.empty();
		this.dom
			.append("<div id=\""+divId+"-messages\"></div>")
			.append("<div id=\""+divId+"-backlink\"></div>")
			.append("<div id=\""+divId+"-content\"></div>");
		
		this.playerView = null;
		this.loadingView = null;
		this.messagesView = new MessagesView(divId+"-messages");
		this.backlinkView = new MessagesView(divId+"-backlink");
		
		//Get URL parameters
		this.urlService = new URLService(
			() => { return $(location).attr('href'); },
			(u) => { window.history.replaceState({}, "Pic4Carto", u); }
		);
		let bboxString = this.urlService.getParameter("bbox");
		let forceRefresh = (this.urlService.getParameter("refresh") == 1);
		
		if(bboxString !== undefined && bboxString.match(/^-?\d+(\.\d+)?(,-?\d+(\.\d+)?){3}$/)) {
			let bboxArray = bboxString.split(',');
			let bbox = L.latLngBounds(L.latLng(bboxArray[1], bboxArray[0]), L.latLng(bboxArray[3], bboxArray[2]));
			
			//Back link
			let backlinkHtml = "<a href=\"index.html#15/"+bbox.getCenter().lat+"/"+bbox.getCenter().lng+"\">&lt; Back to map</a>";
			this.backlinkView.show(backlinkHtml, "info", -1);
			
			//Fetchers
			let fetchers = {
				"mapillary": {
					"name": "Mapillary",
					"img": "img/mapillary.512.png"
				},
				"flickr": {
					"name": "Flickr",
					"img": "img/flickr.512.png"
				},
				"wikicommons": {
					"name": "Wikimedia Commons",
					"img": "img/wikicommons.512.png"
				}
			};
			
			//Display loading view
			this.messagesView.show("Retrieving pictures metadata", "info", -1);
			this.loadingView = new LoadingView(divId+"-content", fetchers);
			
			//Handlers for loading advance
			let dataManager = new DataManager();
			
			dataManager.on("fetcherdone", f => {
				this.loadingView.done(f);
			});
			dataManager.on("fetcherfailed", f => {
				this.loadingView.failed(f);
			});
			
			//Handle case when user want to skip data loading
			this.loadingView.on("skip", () => {
				dataManager.onFetcherDone(true);
			});
			
			//Handler for when data is ready
			dataManager.on("dataready", cells => {
				let myCell = null;
				
				//Exact cell
				if(cells.length == 1) {
					myCell = cells[0];
				}
				//Not exact cell
				else {
					myCell = new Cell(bbox);
					
					for(let cId=0; cId < cells.length; cId++) {
						for(let pId=0; pId < cells[cId].pictures.length; pId++) {
							if(bbox.contains(cells[cId].pictures[pId].coordinates)) {
								myCell.addPicture(cells[cId].pictures[pId]);
							}
						}
					}
					
				}
				
				if(myCell.getPictures().length > 0) {
					//Show player view
					setTimeout(() => {
							this.playerView = new PlayerView(divId+"-content", cells[0]);
							this.messagesView.hide();
						},
						1000
					);
				}
				else {
					this.messagesView.show("No recent pictures found on this area<br />"+backlinkHtml, "alert", -1);
				}
			});
			
			dataManager.ask(bbox, forceRefresh);
		}
		else {
			this.messagesView.show("Invalid bounding box", "error", -1);
		}
	}
};

module.exports = Player;
