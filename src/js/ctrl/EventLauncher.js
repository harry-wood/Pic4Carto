/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * EventLauncher is a class able to launch events on its name.
 * It is made to be extended by other classes.
 */
class EventLauncher {
//CONSTRUCTOR
	constructor() {
		if(this.constructor.name === "EventLauncher") {
			throw new TypeError("ctrl.eventlauncher.cantinstantiate");
		}
		
		this.eventListeners = {};
	}

//OTHER METHODS
	/**
	 * Add a new listener on a given event
	 * @param event The event to listen to
	 * @param handler The handling function
	 */
	on(event, handler) {
		if(this.eventListeners[event] == undefined) {
			this.eventListeners[event] = [];
		}
		this.eventListeners[event].push(handler);
	}
	
	/**
	 * Removes a listener of an event
	 * @param event The concerned event
	 * @param handler (optional) the handler to remove, if no one given removes all handlers for the given event
	 */
	off(event, handler) {
		if(handler == null) {
			this.eventListeners[event] = undefined;
		}
		else if(this.eventListeners[event] != undefined) {
			while(this.eventListeners[event].indexOf(handler) >= 0) {
				this.eventListeners[event].splice(this.eventListeners[event].indexOf(handler), 1);
			}
		}
	}
	
	/**
	 * Fires an event
	 * @param event The event to fire
	 * @param data The associated data
	 */
	fire(event, data) {
		for(let f in this.eventListeners[event]) {
			setTimeout(() => {
				this.eventListeners[event][f](data);
			}, 0);
		}
	}
}

module.exports = EventLauncher;
