/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

require("./Compatibility");
let L = require("leaflet");
let $ = require("jquery");

let Cell = require("../model/Cell");
let DataManager = require("./service/DataManager");
let MainView = require("../view/Main");
let URLService = require("./service/URL");

const MIN_ZOOM_DATA = 14;

/**
 * Main controller manages everything related to main page.
 * In fact, it launches the Main view.
 */
class Main {
//CONSTRUCTOR
	constructor(divId) {
		this.view = new MainView(divId);
		this.dataManager = new DataManager();
		this.timer = null;
		
		//URL parameters
		this.url = new URLService(
			() => { return $(location).attr('href'); },
			(u) => { window.history.replaceState({}, "Pic4Carto", u); }
		);
		
		this.view.areaSelector.map.on("load moveend zoomend", () => {
			//Manage timer for statistics retrieval
			if(this.timer !== null) {
				clearTimeout(this.timer);
			}
			
			this.timer = setTimeout(this.getStatistics.bind(this), 2000);
		});
	}

//OTHER METHODS
	/**
	 * Ask for data retrieval to show map statistics
	 */
	getStatistics() {
		if(this.view.areaSelector.map.getZoom() >= MIN_ZOOM_DATA) {
			//Ask for data retrieval
			this.dataManager.off("statsready");
			this.dataManager.on("statsready", stats => {
				this.view.areaSelector.setLoadingGridData(false);
			});
			this.dataManager.on("statscellready", d => {
				let west, south, east, north;
				[west, south, east, north] = d.bbox.split(',').map(parseFloat);
				this.view.areaSelector.setCellData(new Cell(new L.LatLngBounds(new L.LatLng(south, west), new L.LatLng(north, east))), d.stats);
			});
			
			this.view.areaSelector.setLoadingGridData(true);
			this.dataManager.askStatistics(this.view.areaSelector.map.getBounds(), this.url.getParameter("refresh") == 1);
		}
	}
}

module.exports = {
	init: function(divId) {
		return new Main(divId);
	}
};
