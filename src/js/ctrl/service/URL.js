/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

require("../Compatibility");

//CONSTANTS
const VALID_PARAMETERS = [ "bbox", "refresh" ];

/**
 * URL service reads and updates browser URL according to application parameters.
 * This service only handles query part of URL (see doc/API.md).
 */
class URL {
//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param read The read function, which retrieves the URL
	 * @param write The write function, which changes the URL in browser
	 */
	constructor(read, write) {
		this.readURL = read;
		this.writeURL = write;
		this.url = undefined;	/** Current URL value **/
		this.base = undefined;	/** The base URL **/
	}


//ACCESSORS
	/**
	* Return the wanted parameter
	* @param p The parameter to read
	* @return The wanted value as string
	*/
	getParameter(p) {
		if(this.url == undefined) { this.read(); }
		if(!VALID_PARAMETERS.includes(p)) { throw new Error("ctrl.service.URL.invalidParameter"); }
		
		return this.url[p];
	}


//MODIFIERS
	/**
	* Edits a given parameter
	* @param p The parameter to edit
	* @param v The new value
	*/
	setParameter(p, v) {
		if(this.url == undefined) { this.read(); }
		if(!VALID_PARAMETERS.includes(p)) { throw new Error("ctrl.service.URL.invalidParameter"); }
		
		//Change parameter
		this.url[p] = v;
	}


//OTHER METHODS
	/**
	* Reads the URL from the reader function
	*/
	read() {
		let url = this.readURL();
		
		//Parse given URL
		this.base = (url.indexOf("?") >= 0) ? url.split('?')[0] : url.split('#')[0];
		let query = (url.indexOf("?") >= 0) ? url.split('?')[1].split('#')[0] : "";
		this.url = {};
		
		//Read query parameters
		let params = {};
		let sURLVariables = query.split('&');
		for(let i=0; i < sURLVariables.length; i++) {
			let sParameterName = sURLVariables[i].split('=');
			if(VALID_PARAMETERS.includes(sParameterName[0])) {
				this.url[sParameterName[0]] = sParameterName[1];
			}
		}
	}

	/**
	* Writes the URL into the writer function
	*/
	write() {
		if(this.url == undefined) { this.read(); }
		let url = this.base;
		let query = "";
		
		//Write parameters
		for(let p in this.url) {
			if(this.url[p] != null && this.url[p] != "null") {
				if(query.length > 0) { query += '&'; }
				query += p + "=" + this.url[p];
			}
		}
		
		//Add query part to URL
		if(query.length > 0) {
			url += '?' + query;
		}
		
		//Add current hash part
		let splitHash = this.readURL().split('#');
		if(splitHash.length > 1) {
			url += '#' + splitHash[1];
		}
		
		this.writeURL(url);
	}
}

module.exports = URL;
