/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let L = require("leaflet");

let Cell = require("../../model/Cell");
let CellStore = require("./CellStore");
let EventLauncher = require("../EventLauncher");
let FlickrFetcher = require("../fetchers/Flickr");
let GridFactory = require("../../model/GridFactory");
let MapillaryFetcher = require("../fetchers/Mapillary");
let WikiCommonsFetcher = require("../fetchers/WikiCommons");

const MAPILLARY_CLIENT_ID = "eDhQTTdnRmZJNXdYZWUwRDQxc1NwdzoyMTgzMTUzNTY5YjI3OWZh";
const FLICKR_API_KEY = "b8c50865a2c22d93d3e673de8bd50178";

/**
 * DataManager retrieves pictures metadata from CellStore or directly from pictures providers.
 */
class DataManager extends EventLauncher {
//CONSTRUCTOR
	constructor() {
		super();
		this.cellStore = new CellStore();
		
		//Fetchers definition
		this.fetchers = {
			mapillary: {
				fetcher: new MapillaryFetcher(MAPILLARY_CLIENT_ID),
				done: false
			},
			flickr: {
				fetcher: new FlickrFetcher(FLICKR_API_KEY),
				done: false
			},
			wikicommons: {
				fetcher: new WikiCommonsFetcher(),
				done: false
			}
		};
	}

//OTHER METHODS
	/**
	 * Ask for pictures metadata retrieval.
	 * Then, you might subscribe to events in order to get data.
	 * @param bbox The area in which data must be retrieved
	 * @param fresh Force download from the web (optional, false if undefined)
	 */
	ask(bbox, fresh) {
		fresh = fresh || false;
		this.result = GridFactory.make(bbox);
		this.cachedCellIds = [];
		
		/*
		 * Retrieve cached data
		 */
		let fetchersBbox = bbox;
		
		if(!fresh) {
			fetchersBbox = null;
			
			for(let i=0; i < this.result.length; i++) {
				let cachedCell = this.cellStore.get(this.result[i].getId());
				
				//If cell exists and recent enough, store it in result
				if(cachedCell !== null && cachedCell.creationDate >= Date.now() - 6 * 60 * 60 * 1000) {
					this.result[i] = cachedCell;
					this.cachedCellIds.push(i);
				}
				//Add cell area in fetchers bbox
				else {
					if(fetchersBbox == null) {
						fetchersBbox = this.result[i].bounds;
					}
					else {
						fetchersBbox.extend(this.result[i].bounds);
					}
				}
			}
		}
		else {
			this.cellStore.clear();
		}
		
		/*
		 * Retrieve data from fetchers
		 */
		if(this.cachedCellIds.length < this.result.length) {
			this.rawPictures = [];
			
			//Call fetchers for pictures metadata retrieval
			for(let f in this.fetchers) {
				let fId = f;
				let fetcher = this.fetchers[f];
				fetcher.done = false;
				
				fetcher.fetcher.on("done", pics => {
					this.rawPictures = this.rawPictures.concat(pics);
					this.fire("fetcherdone", fId);
					this.fetchers[fId].done = true;
					this.onFetcherDone();
				});
				
				fetcher.fetcher.on("failed", e => {
					console.log(e);
					this.fire("fetcherfailed", fId);
					this.fetchers[fId].done = true;
					this.onFetcherDone();
				});
				
				fetcher.fetcher.get(fetchersBbox);
			}
		}
		else {
			this.fire("dataready", this.result);
		}
	}
	
	/**
	 * Ask for statistics in a given area.
	 * Then, you might subscribe to events in order to get data.
	 * @param bbox The area in which data must be retrieved
	 * @param fresh Force download from the web (optional, false if undefined)
	 */
	askStatistics(bbox, fresh) {
		fresh = fresh || false;
		let cells = GridFactory.make(bbox);
		let cellsForFetchers = [];
		this.rawStats = {};
		this.statsFetchersCalls = 0;
		this.statsDoneTimer = undefined;
		
		//Retrieve data from cache
		if(!fresh) {
			for(let cId in cells) {
				if(cells.hasOwnProperty(cId)) {
					let cell = cells[cId];
					let cellCache = this.cellStore.getStatistics(cell.bounds.toBBoxString());
					
					//If cached, put in results
					if(cellCache !== null) {
						this.rawStats[cell.bounds.toBBoxString()] = cellCache;
						this.rawStats[cell.bounds.toBBoxString()].cached = true;
						this.fire("statscellready", { bbox: cell.bounds.toBBoxString(), stats: cellCache });
					}
					//Else, add cell to fetcher download list
					else {
						cellsForFetchers.push(cell);
					}
				}
			}
		}
		else {
			cellsForFetchers = cells;
		}
		
		if(cellsForFetchers.length > 0) {
			//Set handlers on fetchers
			for(let f in this.fetchers) {
				let fetcher = this.fetchers[f];
				fetcher.fetcher.on("donelight", this.fetcherDoneHandler.bind(this));
				fetcher.fetcher.on("failed", this.fetcherFailedHandler.bind(this));
			}
			
			//Loop over each cell
			for(let cId in cellsForFetchers) {
				if(cellsForFetchers.hasOwnProperty(cId)) {
					let cell = cellsForFetchers[cId];
					
					//Call each fetcher over this cell
					for(let f in this.fetchers) {
						let fetcher = this.fetchers[f];
						this.statsFetchersCalls++;
						fetcher.fetcher.getLight(cell.bounds);
					}
				}
			}
		}
		else {
			this.allFetchersDoneHandler();
		}
	}
	
	/**
	 * Called to handle case when a fetcher has done its work
	 * @param skipFetcherCheck Skips the check if all fetchers have done their job (useful when skipping loading)
	 */
	onFetcherDone(skipFetcherCheck) {
		skipFetcherCheck = skipFetcherCheck || false;
		
		//Check all fetchers have done their job
		for(let f in this.fetchers) {
			if(!skipFetcherCheck) {
				if(this.fetchers[f].done === false) {
					return false;
				}
			}
			else {
				this.fetchers[f].fetcher.off("done");
				this.fetchers[f].fetcher.off("failed");
			}
		}
		
		//If so, update cells
		for(let i=0; i < this.rawPictures.length; i++) {
			let coords = this.rawPictures[i].coordinates;
			let foundCell = null;
			let cellId = 0;
			
			//Search the cell containing the current picture
			while(foundCell == null && cellId < this.result.length) {
				if(this.result[cellId].bounds.contains(coords)) {
					foundCell = this.result[cellId];
				}
				else {
					cellId++;
				}
			}
			
			if(foundCell) {
				//If the pictures is on a cached cell, we renew completely the cell
				if(this.cachedCellIds.indexOf(cellId) >= 0) {
					this.result[cellId] = new Cell(foundCell.bounds);
					foundCell = this.result[cellId];
					this.cachedCellIds.splice(this.cachedCellIds.indexOf(cellId), 1);
				}
				
				//Add picture to cell
				foundCell.addPicture(this.rawPictures[i]);
			}
		}
		
		//Then save cells to cellstore
		for(let i=0; i < this.result.length; i++) {
			this.cellStore.update(this.result[i]);
		}
		
		//Fire event saying data is ready
		this.fire("dataready", this.result);
	}
	
	/**
	 * Handler of fetcher done in getStatistics
	 */
	fetcherDoneHandler(stats) {
		if(this.statsDoneTimer !== undefined) {
			clearTimeout(this.statsDoneTimer);
			this.statsDoneTimer = undefined;
		}
		
		if(!this.rawStats[stats.bbox]) {
			this.rawStats[stats.bbox] = { f: 1, triggered: false };
		}
		
		//Change last picture date
		if(!this.rawStats[stats.bbox].last || this.rawStats[stats.bbox].last < stats.last) {
			this.rawStats[stats.bbox].last = stats.last;
		}
		
		//Add amount of pictures
		if(!this.rawStats[stats.bbox].amount) {
			this.rawStats[stats.bbox].amount = [ stats.amount ];
		}
		else {
			this.rawStats[stats.bbox].amount.push(stats.amount);
		}
		
		this.statsFetchersCalls--;
		this.rawStats[stats.bbox].f++;
		
		if(this.rawStats[stats.bbox].f == Object.keys(this.fetchers).length) {
			this.rawStats[stats.bbox].triggered = true;
			this.fire("statscellready", { bbox: stats.bbox, stats: this.rawToCleanStats(this.rawStats[stats.bbox]) });
		}
		this.statsDoneTimer = setTimeout(this.allFetchersDoneHandler.bind(this), 100);
	}
	
	/**
	 * Handler of fetcher failed in getStatistics
	 */
	fetcherFailedHandler(e) {
		if(this.statsDoneTimer !== undefined) {
			clearTimeout(this.statsDoneTimer);
			this.statsDoneTimer = undefined;
		}
		
		if(typeof e === "string" && e.length > 0) {
			console.log(e);
		}
		this.statsFetchersCalls--;
		
		this.statsDoneTimer = setTimeout(this.allFetchersDoneHandler.bind(this), 100);
	}
	
	/**
	 * Handler for all fetchers done in getStatistics
	 */
	allFetchersDoneHandler() {
		if(this.statsFetchersCalls === 0) {
			//Convert amount values
			for(let s in this.rawStats) {
				let stat = this.rawStats[s];
				let triggered = stat.triggered;
				if(!stat.cached) {
					stat = this.rawToCleanStats(stat);
					this.rawStats[s] = stat;
					
					//Update cache
					this.cellStore.updateStatistics(s, stat);
					
					if(!triggered) {
						this.fire("statscellready", { bbox: s, stats: stat });
					}
				}
			}
			
			//Fire event
			this.fire("statsready", this.rawStats);
			
			//Clear events
			for(let f in this.fetchers) {
				let fetcher = this.fetchers[f];
				fetcher.fetcher.off("donelight");
				fetcher.fetcher.off("failed");
			}
		}
	}
	
	/**
	 * Converts raw statistics object into clean statistics object
	 */
	rawToCleanStats(raw) {
		let clean = { last: raw.last, amount: 0, approxAmount: false };
		
		for(let a=0; a < raw.amount.length; a++) {
			clean.approxAmount = clean.approxAmount || raw.amount[a].charAt(0) == ">";
			clean.amount += parseInt(raw.amount[a].substring(1));
		}
		
		return clean;
	}
}

module.exports = DataManager;
