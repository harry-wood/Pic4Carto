/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let $ = require("jquery");
let L = require("leaflet");
let Fetcher = require("../Fetcher");
let Picture = require("../../model/Picture");

/**
 * Mapillary fetcher
 */
class Mapillary extends Fetcher {
//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param clientId The client ID for the Mapillary API
	 */
	constructor(clientId) {
		super();
		if(typeof clientId === "string" && clientId.length > 0) {
			this.clientId = clientId;
		}
		else {
			throw new Error("ctrl.fetchers.mapillary.invalidclientid");
		}
	}

//ACCESSORS
	/**
	 * @return This provider name (Mapillary)
	 */
	get name() {
		return "Mapillary";
	}
	
	/**
	 * Get all the images in the given area for this provider.
	 * Subscribe to the "done" event to get pictures when ready
	 * @param bbox The bounding box
	 */
	get(bbox) {
		this.bbox = L.latLngBounds(bbox.getSouthWest().wrap(), bbox.getNorthEast().wrap());
		this.date = Date.now() - 6 * 30 * 24 * 60 * 60 * 1000;
		this.picsPerRequest = 100;
		this.page = 0;
		this.pictures = [];
		
		this.download();
	}
	
	/**
	 * Get a summary of what the provider can return in the given bounds.
	 * Subscribe to the "donelight" event to get statistics when ready
	 * @param bbox The bounding box
	 */
	getLight(bbox) {
		let date = Date.now() - 6 * 30 * 24 * 60 * 60 * 1000;
		
		let url = "https://a.mapillary.com/v2/search/im?client_id="
					+ this.clientId
					+ "&max_lat=" + bbox.getNorth()
					+ "&max_lon=" + bbox.getEast()
					+ "&min_lat=" + bbox.getSouth()
					+ "&min_lon=" + bbox.getWest()
					+ "&limit=100"
					+ "&start_time=" + date;
		
		$.ajax({
			url: url,
			dataType: "json",
			success: (d) => { this.parseLight(d, bbox); },
			error: this.fail.bind(this)
		});
	}

//OTHER METHODS
	/**
	 * Retrieves pictures metadata for current bounding box, page and date
	 */
	download() {
		let url = "https://a.mapillary.com/v2/search/im?client_id="
					+ this.clientId
					+ "&max_lat=" + this.bbox.getNorth()
					+ "&max_lon=" + this.bbox.getEast()
					+ "&min_lat=" + this.bbox.getSouth()
					+ "&min_lon=" + this.bbox.getWest()
					+ "&limit=" + this.picsPerRequest
					+ "&start_time=" + this.date
					+ "&page=" + this.page;
		
		$.ajax({
			url: url,
			dataType: "json",
			success: this.parse.bind(this),
			error: this.fail.bind(this)
		});
	}
	
	/**
	 * Parses the result of the download function
	 * @param result The returned data from the web
	 */
	parse(result) {
		if(result === null || result.ims === undefined) {
			this.fail(null, null, new Error("ctrl.fetcher.mapillary.getfailed"));
		}
		
		//Parse data
		if(result.ims && result.ims.length > 0) {
			for(let i=0; i < result.ims.length; i++) {
				let pic = result.ims[i];
				this.pictures.push(new Picture(
					"https://d1cuyjsrcm0gby.cloudfront.net/" + pic.key + "/thumb-2048.jpg",
					pic.captured_at,
					L.latLng(pic.lat, pic.lon),
					this.name,
					pic.user,
					"CC By-SA 4.0",
					"https://www.mapillary.com/app/?pKey=" + pic.key + "&lat=" + pic.lat + "&lng=" + pic.lng + "&focus=photo",
					pic.ca
				));
			}
			
			this.page = (result.ims.length == this.picsPerRequest) ? this.page + 1 : -1;
		}
		else {
			this.page = -1;
		}
		
		//Next step
		if(this.page >= 0) {
			this.download();
		}
		else {
			this.fire("done", this.pictures);
		}
	}
	
	/**
	 * Parses the result of the getLight function
	 * @param data The returned data from the web
	 * @param bbox The bounding box of given data
	 */
	parseLight(data, bbox) {
		if(data === null || data.ims === undefined) {
			this.fail(null, null, new Error("ctrl.fetcher.mapillary.getlightfailed"));
		}
		
		//Parse data
		if(data.ims && data.ims.length > 0) {
			//Search most recent picture date
			data.ims.sort((a,b) => { return b.captured_at - a.captured_at; });
			
			this.fire("donelight", {
				last: data.ims[0].captured_at,
				amount: (data.ims.length < 100) ? "e"+data.ims.length : ">100",
				bbox: bbox.toBBoxString()
			});
		}
		else {
			this.fire("donelight", {
				amount: "e0",
				bbox: bbox.toBBoxString()
			});
		}
	}
}

module.exports = Mapillary;
