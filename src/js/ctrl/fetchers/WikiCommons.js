/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let $ = require("jquery");
let L = require("leaflet");
let Fetcher = require("../Fetcher");
let Picture = require("../../model/Picture");

/**
 * Wikimedia Commons fetcher
 */
class WikiCommons extends Fetcher {
//CONSTRUCTOR
	/**
	 * Class constructor
	 */
	constructor() {
		super();
	}

//ACCESSORS
	/**
	 * @return This provider name (Wikimedia Commons)
	 */
	get name() {
		return "Wikimedia Commons";
	}
	
	/**
	 * Get all the images in the given area for this provider.
	 * Subscribe to the "done" event to get pictures when ready
	 * @param bbox The bounding box
	 */
	get(bbox) {
		this.bbox = L.latLngBounds(bbox.getSouthWest().wrap(), bbox.getNorthEast().wrap());
		this.date = Date.now() - 6 * 30 * 24 * 60 * 60 * 1000;
		this.picsPerRequest = 100;
		this.pictures = [];
		this.continue = null;
		
		this.download();
	}
	
	/**
	 * Get a summary of what the provider can return in the given bounds.
	 * Subscribe to the "donelight" event to get statistics when ready
	 * @param bbox The bounding box
	 */
	getLight(bbox) {
		let date = Date.now() - 6 * 30 * 24 * 60 * 60 * 1000;
		
		let url = "https://commons.wikimedia.org/w/api.php?action=query&format=json&origin=*&prop=imageinfo"
					+ "&generator=geosearch&iiprop=timestamp|metadata"
					+ "&ggsbbox=" + bbox.getNorth() + "|" + bbox.getWest() + "|" + bbox.getSouth() + "|" + bbox.getEast()
					+ "&ggslimit=100"
					+ "&iilimit=99"
					+ "&ggsnamespace=6";
		
		$.ajax({
			url: url,
			dataType: "json",
			success: (d) => { this.parseLight(d, bbox, date); },
			error: this.fail.bind(this)
		});
	}


//OTHER METHODS
	/**
	 * Retrieves pictures metadata for current bounding box, page and date
	 */
	download() {
		let url = "https://commons.wikimedia.org/w/api.php?action=query&format=json&origin=*&prop=coordinates|imageinfo"
			+ "&continue=" + ((this.continue && this.continue.continue) ? this.continue.continue : "")
			+ ((this.continue && this.continue.cocontinue) ? "&cocontinue=" + this.continue.cocontinue : "")
			+ "&generator=geosearch&iiprop=timestamp|user|url|extmetadata|metadata|size&iiextmetadatafilter=LicenseShortName"
			+ "&ggsbbox=" + this.bbox.getNorth() + "|" + this.bbox.getWest() + "|" + this.bbox.getSouth() + "|" + this.bbox.getEast()
			+ "&ggslimit=" + this.picsPerRequest
			+ "&iilimit=" + (this.picsPerRequest-1)
			+ "&colimit=" + (this.picsPerRequest-1)
			+ "&ggsnamespace=6&&iimetadataversion=latest";
		
		$.ajax({
			url: url,
			dataType: "json",
			success: this.parse.bind(this),
			error: this.fail.bind(this)
		});
	}
	
	/**
	 * Parses the result of the download function
	 * @param result The returned data from the web
	 */
	parse(result) {
		//Parse data
		if(result.query && result.query.pages && Object.keys(result.query.pages).length > 0) {
			for(let picId in result.query.pages) {
				let pic = result.query.pages[picId];
				
				if(
					pic.imageinfo
					&& pic.imageinfo.length > 0
					&& pic.coordinates
					&& pic.coordinates.length > 0
					&& pic.imageinfo[0].url
					&& pic.imageinfo[0].timestamp
					&& pic.coordinates[0].lat
					&& pic.coordinates[0].lon
					&& pic.imageinfo[0].user
					&& pic.imageinfo[0].extmetadata.LicenseShortName
				) {
					//Try to find DateTime in metadata
					let date = null;
					if(pic.imageinfo[0].metadata && pic.imageinfo[0].metadata.length > 0) {
						let mdId = 0;
						while(mdId < pic.imageinfo[0].metadata.length && date == null) {
							if(pic.imageinfo[0].metadata[mdId].name == "DateTime") {
								date = (new Date(pic.imageinfo[0].metadata[mdId].value)).getTime();
							}
							mdId++;
						}
					}
					if(date == null) { date = (new Date(pic.imageinfo[0].timestamp)).getTime(); }
					
					if(date >= this.date) {
						console.log(this.toThumbURL(pic.imageinfo[0].url));
						this.pictures.push(new Picture(
							(pic.imageinfo[0].width && pic.imageinfo[0].width > 1024) ? this.toThumbURL(pic.imageinfo[0].url) : pic.imageinfo[0].url,
							date,
							L.latLng(pic.coordinates[0].lat, pic.coordinates[0].lon),
							this.name,
							pic.imageinfo[0].user,
							pic.imageinfo[0].extmetadata.LicenseShortName.value,
							pic.imageinfo[0].descriptionurl
						));
					}
				}
			}
			
			this.continue = (result.continue && (result.continue.cocontinue || result.continue.continue)) ? result.continue : null;
		}
		else {
			this.continue = null;
		}
		
		//Next step
		if(this.continue !== null) {
			this.download();
		}
		else {
			this.fire("done", this.pictures);
		}
	}
	
	/**
	 * Parses the result of the getLight function
	 * @param data The returned data from the web
	 * @param bbox The bounding box of given data
	 * @param date The start date
	 */
	parseLight(data, bbox, date) {
		//Parse data
		if(data.query) {
			if(data.query.pages && Object.keys(data.query.pages).length > 0) {
				let lastDate = null;
				let picAmount = 0;
				for(let picId in data.query.pages) {
					let pic = data.query.pages[picId];
					
					if(
						pic.imageinfo
						&& pic.imageinfo.length > 0
						&& pic.imageinfo[0].timestamp
					) {
						//Try to find DateTime in metadata
						let picDate = null;
						if(pic.imageinfo[0].metadata && pic.imageinfo[0].metadata.length > 0) {
							let mdId = 0;
							while(mdId < pic.imageinfo[0].metadata.length && picDate == null) {
								if(pic.imageinfo[0].metadata[mdId].name == "DateTime") {
									picDate = (new Date(pic.imageinfo[0].metadata[mdId].value)).getTime();
								}
								mdId++;
							}
						}
						if(picDate == null) { picDate = (new Date(pic.imageinfo[0].timestamp)).getTime(); }
						
						if(picDate >= date) {
							picAmount++;
							if(lastDate == null) { lastDate = picDate; }
							else if(lastDate <= picDate) { lastDate = picDate; }
						}
					}
				}
				if(picAmount > 0) {
					this.fire("donelight", {
						last: lastDate,
						amount: ">"+picAmount,
						bbox: bbox.toBBoxString()
					});
				}
				else {
					this.fire("donelight", {
						amount: "e0",
						bbox: bbox.toBBoxString()
					});
				}
			}
			else {
				this.fire("donelight", {
					amount: "e0",
					bbox: bbox.toBBoxString()
				});
			}
		}
		else if(data.batchcomplete === "") {
			this.fire("donelight", {
				amount: "e0",
				bbox: bbox.toBBoxString()
			});
		}
		else {
			this.fail(null, null, new Error("ctrl.fetcher.wikicommons.getlightfailed"));
		}
	}
	
	/**
	 * Convert an URL of a WikiCommons image into its thumbnail
	 * @param url The image URL
	 * @return The thumbnail URL
	 */
	toThumbURL(url) {
		let rgx = /^(.+wikipedia\/commons)\/([a-zA-Z0-9]\/[a-zA-Z0-9]{2})\/(.+)$/;
		let rgxRes = rgx.exec(url);
		return rgxRes[1] + "/thumb/" + rgxRes[2] + "/" + rgxRes[3] + "/1024px-" + rgxRes[3];
	}
}

module.exports = WikiCommons;
