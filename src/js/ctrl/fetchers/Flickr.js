/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let $ = require("jquery");
let L = require("leaflet");
let Fetcher = require("../Fetcher");
let Picture = require("../../model/Picture");

/**
 * Flickr fetcher
 */
class Flickr extends Fetcher {
//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param apiKey The API key for Flickr
	 */
	constructor(apiKey) {
		super();
		if(typeof apiKey === "string" && apiKey.length > 0) {
			this.apiKey = apiKey;
		}
		else {
			throw new Error("ctrl.fetchers.flickr.invalidapikey");
		}
	}

//ACCESSORS
	/**
	 * @return This provider name (Flickr)
	 */
	get name() {
		return "Flickr";
	}
	
	/**
	 * Get all the images in the given area for this provider.
	 * Subscribe to the "done" event to get pictures when ready.
	 * @param bbox The bounding box
	 */
	get(bbox) {
		this.bbox = L.latLngBounds(bbox.getSouthWest().wrap(), bbox.getNorthEast().wrap());
		this.date = (new Date(Date.now() - 6 * 30 * 24 * 60 * 60 * 1000)).toISOString().split('T')[0];
		this.picsPerRequest = 100;
		this.page = 1;
		this.pictures = [];
		
		this.downloadLicenses();
	}
	
	/**
	 * Get a summary of what the provider can return in the given bounds.
	 * Subscribe to the "donelight" event to get statistics when ready
	 * @param bbox The bounding box
	 */
	getLight(bbox) {
		let date = (new Date(Date.now() - 6 * 30 * 24 * 60 * 60 * 1000)).toISOString().split('T')[0];
		let url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="
					+ this.apiKey
					+ "&min_taken_date=" + date
					+ "&bbox=" + bbox.getWest() + "%2C" + bbox.getSouth() + "%2C" + bbox.getEast() + "%2C" + bbox.getNorth()
					+ "&has_geo=1"
					+ "&per_page=1"
					+ "&license=1,2,3,4,5,6,7,8,9,10"
					+ "&format=json&nojsoncallback=1&extras=date_taken";
		
		$.ajax({
			url: url,
			dataType: "json",
			success: (d) => { this.parseLight(d, bbox); },
			error: this.fail.bind(this)
		});
	}

//OTHER METHODS
	/**
	 * Retrieves pictures metadata for current bounding box, page and date
	 */
	downloadLicenses() {
		$.ajax({
			url: "https://api.flickr.com/services/rest/?method=flickr.photos.licenses.getInfo&api_key="+this.apiKey+"&format=json&nojsoncallback=1",
			dataType: "json",
			success: this.parseLicenses.bind(this),
			error: this.fail.bind(this)
		});
	}
	
	/**
	 * Analyses the data from license API call
	 */
	parseLicenses(d) {
		this.licenses = {};
		if(d.stat && d.stat == "ok" && d.licenses && d.licenses.license) {
			for(let i=0; i < d.licenses.license.length; i++) {
				let lcs = d.licenses.license[i];
				this.licenses[lcs.id] = lcs.name;
			}
			
			this.download();
		}
		else {
			this.fail(null, null, new Error("ctrl.fetcher.flickr.invalidlicensedata"));
		}
	}
	
	/**
	 * Retrieves pictures metadata for current bounding box, page and date
	 */
	download() {
		let licenseList = Object.keys(this.licenses).filter((v) => { return v >= 4 && v != 6; });
		
		let url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="
			+ this.apiKey
			+ "&min_taken_date=" + this.date
			+ "&bbox=" + this.bbox.getWest() + "%2C" + this.bbox.getSouth() + "%2C" + this.bbox.getEast() + "%2C" + this.bbox.getNorth()
			+ "&has_geo=1"
			+ "&per_page=" + this.picsPerRequest
			+ "&page=" + this.page
			+ "&license=" + licenseList.join(",")
			+ "&format=json&nojsoncallback=1&extras=license%2Cdate_taken%2Cowner_name%2Cgeo%2Curl_l";
		
		$.ajax({
			url: url,
			dataType: "json",
			success: this.parse.bind(this),
			error: this.fail.bind(this)
		});
	}
	
	/**
	 * Parses the result of the download function
	 * @param result The returned data from the web
	 */
	parse(result) {
		if(result === null || result.photos === undefined || result.photos.photo === undefined) {
			this.fail(null, null, new Error("ctrl.fetcher.flickr.getfailed"));
		}
		
		//Parse data
		if(result.stat && result.stat === "ok" && result.photos.photo && result.photos.photo.length > 0) {
			for(let i=0; i < result.photos.photo.length; i++) {
				let pic = result.photos.photo[i];
				
				if(pic.url_l && pic.datetaken && pic.latitude && pic.longitude && pic.license > 0) {
					this.pictures.push(new Picture(
						pic.url_l,
						(new Date(pic.datetaken.replace(" ", "T"))).getTime(),
						L.latLng(pic.latitude, pic.longitude),
						this.name,
						pic.ownername,
						this.licenses[pic.license],
						"https://www.flickr.com/photos/"+pic.owner+"/"+pic.id
					));
				}
			}
			
			this.page = (result.photos.photo.length == this.picsPerRequest) ? this.page + 1 : -1;
		}
		else {
			this.page = -1;
		}
		
		//Next step
		if(this.page > 0) {
			this.download();
		}
		else {
			this.fire("done", this.pictures);
		}
	}

	/**
	 * Parses the result of the getLight function
	 * @param data The returned data from the web
	 * @param bbox The bounding box of given data
	 */
	parseLight(data, bbox) {
		if(data === null || data.photos === undefined || data.photos.total === undefined || data.photos.photo === undefined) {
			this.fail(null, null, new Error("ctrl.fetcher.flickr.getlightfailed"));
		}
		
		//Parse data
		if(parseInt(data.photos.total) > 0) {
			this.fire("donelight", {
				last: (new Date(data.photos.photo[0].datetaken.replace(" ", "T"))).getTime(),
				amount: "e"+data.photos.total,
				bbox: bbox.toBBoxString()
			});
		}
		else {
			this.fire("donelight", {
				amount: "e0",
				bbox: bbox.toBBoxString()
			});
		}
	}
}

module.exports = Flickr;

