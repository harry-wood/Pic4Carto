/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let EventLauncher = require("./EventLauncher");

/**
 * A fetcher is an object which will download all pictures of a given provider, in a given area.
 * This is an abstract class, it must be extended and the methods overridden.
 */
class Fetcher extends EventLauncher {
//CONSTRUCTOR
	constructor() {
		super();
		
		if(this.constructor.name === "Fetcher") {
			throw new TypeError("ctrl.fetcher.cantinstantiate");
		}
		
		if(this.get === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.get");
		}
		
		if(this.getLight === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.getlight");
		}
		
		if(this.name === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.getName");
		}
	}

//OTHER METHODS
	/**
	 * Handles error when download fails
	 */
	fail(xhr, textStatus, errorThrown) {
		this.fire("failed", errorThrown);
	}
}

module.exports = Fetcher;
