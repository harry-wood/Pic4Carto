/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A picture is an image took by an author, made available freely by a given provider.
 * It has coordinates associated, and some other metadata.
 */
class Picture {
//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param pictureUrl The picture URL
	 * @param date The date where picture was taken, in Unix time (milliseconds)
	 * @param coords The coordinates where picture was taken
	 * @param provider The provider of the given picture
	 * @param author (optional) The author of the picture
	 * @param license (optional) The license of this picture
	 * @param detailsUrl (optional) URL where more details about the picture can be found
	 * @param direction (optional) The direction (0-360°, 0 = North, 90 = East, 180 = South, 270 = West) of the picture
	 */
	constructor(pictureUrl, date, coords, provider, author, license, detailsUrl, direction) {
		//Mandatory parameters
		if(typeof pictureUrl === "string" && pictureUrl.length > 0) {
			this.pictureUrl = pictureUrl;
		}
		else {
			throw new Error("model.picture.invalid.pictureUrl");
		}
		
		if(typeof date === "number" && date >= 0) {
			this.date = date;
		}
		else {
			throw new Error("model.picture.invalid.date");
		}
		
		if(coords !== null && coords !== undefined && typeof coords.lat === "number" && typeof coords.lng === "number") {
			this.coordinates = coords;
		}
		else {
			throw new Error("model.picture.invalid.coords");
		}
		
		if(typeof provider === "string" && provider.length > 0) {
			this.provider = provider;
		}
		else {
			throw new Error("model.picture.invalid.provider");
		}
		
		//Optional parameters
		this.author = author || null;
		this.license = license || null;
		this.detailsUrl = detailsUrl || null;
		this.direction = (typeof direction === "number" && direction >=0 && direction < 360) ? direction : null;
	}
}

module.exports = Picture;
