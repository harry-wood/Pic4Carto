/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let $ = require("jquery");

/**
 * Loading view shows the state of loading of the several pictures fetchers.
 */
class Loading {
//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param divId The DIV ID in DOM
	 * @param sources The different sources, as an object { "SourceID": { img: "Image URL", name: "Source name" } }
	 */
	constructor(divId, sources) {
		this.dom = $("#"+divId);
		this.sources = sources;
		
		//Construct DOM
		this.dom.empty();
		this.dom.addClass("p4c-loading");
		this.dom.append("<div class=\"table\"></div>");
		this.dom.append("<div class=\"pass\"><a><img src=\"img/skip.128.png\" title=\"Don't wait for load end\" /> Skip</a></div>");
		this.dom.find(".table").append("<div class=\"row\"></div>");
		this.dom.find(".pass a").click(() => {
			this.dom.trigger("skip");
			for(let s in this.sources) {
				if(!this.sources[s].status) {
					this.sources[s].status = "skipped";
					this.dom
						.find("#src-"+s)
						.find(".source-status img")
						.attr("src", "img/skip.128.png")
						.attr("alt", "Pictures skipped")
						.attr("title", "Pictures skipped");
				}
			}
		} );
		
		for(let s in this.sources) {
			let source = this.sources[s];
			this.dom.find(".row").append(
				"<div id=\"src-"+s+"\" class=\"source\">"
					+ "<div class=\"source-name\">"
						+ "<img src=\""+source.img+"\" alt=\""+source.name+"\" title=\""+source.name+"\" />"
					+ "</div>"
					+ "<div class=\"source-status\">"
						+ "<img src=\"img/spinner_photo.gif\" alt=\"Loading\" title=\"Loading\" />"
					+ "</div>"
				+ "</div>"
			);
		}
	}

//MODIFIERS
	/**
	 * Sets the given source status as done
	 * @param source The source name. If not defined, set all sources as done
	 */
	done(source) {
		if(source) {
			this.sources[source].status = "done";
			
			this.dom
				.find("#src-"+source)
				.find(".source-status img")
				.attr("src", "img/ok.128.png")
				.attr("alt", "Pictures retrieved")
				.attr("title", "Pictures retrieved");
			
			this.manageEvents();
		}
		else {
			for(let s in this.sources) {
				this.done(s);
			}
		}
	}
	
	/**
	 * Sets the given source status as failed
	 * @param source The source name
	 */
	failed(source) {
		this.sources[source].status = "failed";
		
		this.dom
			.find("#src-"+source)
			.find(".source-status img")
			.attr("src", "img/notok.128.png")
			.attr("alt", "Download failed")
			.attr("title", "Download failed");
		
		this.manageEvents();
	}

//OTHER METHODS
	/**
	 * Sends event when loading is complete
	 */
	manageEvents() {
		//Check if all sources have a defined status
		for(let s in this.sources) {
			if(this.sources[s].status === undefined) {
				return null;
			}
		}
		
		//If so, send event saying everything is loaded
		setTimeout(() => { this.dom.trigger("completed"); }, 1000);
	}
	
	on(event, handler) {
		return this.dom.on(event, handler);
	}
}

module.exports = Loading;
