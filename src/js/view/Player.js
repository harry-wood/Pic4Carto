/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let $ = require("jquery");

let Control = require("./Control");
let Description = require("./Description");
let Location = require("./Location");
let WhatsNext = require("./WhatsNext");

/**
 * Player view handles playing pictures.
 * It creates the DOM structure and associates appropriate widgets.
 */
class Player {
//CONSTRUCTOR
	constructor(divId, cell) {
		this.dom = $("#"+divId);
		this.pictures = cell.getPictures();
		this.currentPicId = 0;
		this.timer = null;
		this.imageLoading = false;
		this.hasImageError = false;
		this.imgRotation = 0;
		
		//Create DOM structure
		this.dom.empty();
		this.dom.addClass("p4c-player");
		this.dom
			.append("<div class=\"image\"><img /></div>")
			.append("<div id=\""+divId+"-description\"></div>")
			.append("<div id=\""+divId+"-control\"></div>")
			.append("<div id=\""+divId+"-location\"></div>")
			.append("<div class=\"spinner\"><img src=\"img/spinner_photo.gif\" /></div>")
			.append("<div id=\""+divId+"-whatsnext\"></div>");
		
		//Init widgets
		this.wDescription = new Description(divId+"-description");
		
		this.wLocation = new Location(divId+"-location", cell);
		this.wLocation.map.invalidateSize(false);
		this.wLocation.setCurrentPicture(this.currentPicId);
		
		this.wControl = new Control(divId+"-control");
		this.wWhatsNext = new WhatsNext(divId+"-whatsnext", cell);
		this.on("lastpicture", () => { this.wWhatsNext.show(); });
		
		//Bind control widgets events to player functions
		this.wControl.on("play", this.play.bind(this));
		this.wControl.on("pause", this.pause.bind(this));
		this.wControl.on("prev", this.prev.bind(this));
		this.wControl.on("next", this.next.bind(this));
		this.wControl.on("edit-id", this.editId.bind(this));
		this.wControl.on("edit-josm", this.editJOSM.bind(this));
		this.wControl.on("rotate", () => {
			this.rotate(this.imgRotation+90);
		});
		
		//Expand map button
		this.dom.find("#"+divId+"-location").append("<div class=\"expand-button\"><img src=\"img/expand.64.png\" class=\"expand\" alt=\"+\" title=\"Expand map\" /><img src=\"img/minimize.64.png\" class=\"minimize hide\" alt=\"-\" title=\"Minimize map\" /></div>");
		this.dom.find("#"+divId+"-location .expand-button img.minimize").click(() => {
			this.dom.find("#"+divId+"-location .expand-button img.minimize").addClass("hide");
			this.dom.find("#"+divId+"-location .expand-button img.expand").removeClass("hide");
			this.dom.find("#"+divId+"-location").removeClass("enlarge");
			this.wLocation.map.invalidateSize();
		});
		this.dom.find("#"+divId+"-location .expand-button img.expand").click(() => {
			this.dom.find("#"+divId+"-location .expand-button img.expand").addClass("hide");
			this.dom.find("#"+divId+"-location .expand-button img.minimize").removeClass("hide");
			this.dom.find("#"+divId+"-location").addClass("enlarge");
			this.wLocation.map.invalidateSize();
		});
		
		//Location map picture click handler
		this.wLocation.on("click", (p) => {
			this.pause();
			this.currentPicId = p.pictureId;
			this.showPicture();
		});
		
		//Init picture display
		this.showPicture();
	}

//MODIFIERS
	/**
	 * Starts the picture playing
	 */
	play() {
		if(!this.next()) {
			this.pause();
		}
	}
	
	/**
	 * Pause the picture playing
	 */
	pause() {
		clearInterval(this.timer);
		this.timer = null;
		this.wControl.stopPlaying();
		this.dom.find(".image img").off("load");
	}
	
	/**
	 * Switch to next picture
	 * @return true if gone to next picture
	 */
	next() {
		if(this.currentPicId + 1 < this.pictures.length) {
			this.currentPicId++;
			this.showPicture();
			this.wWhatsNext.hide();
			return true;
		}
		else {
			this.dom.trigger("lastpicture");
			return false;
		}
	}
	
	/**
	 * Switch to previous picture
	 * @return true if gone to previous picture
	 */
	prev() {
		if(this.currentPicId - 1 >= 0) {
			this.currentPicId--;
			this.showPicture();
			this.wWhatsNext.hide();
			return true;
		}
		return false;
	}
	
	/**
	 * Updates display of picture, description and location according to currentPicId value.
	 */
	showPicture() {
		this.dom.find(".spinner img").show();
		this.imageLoading = true;
		
		//Case of previous image failed loading
		if(this.hasImageError) {
			this.fixImgError();
		}
		
		//Edit location and description
		this.wDescription.set(this.pictures[this.currentPicId]);
		this.wLocation.setCurrentPicture(this.currentPicId);
		
		//Change image
		this.dom.find(".image img")
			.attr("src", this.pictures[this.currentPicId].pictureUrl);
		
		//Wait for end of loading
		this.dom.find(".image img")
			.off("load")
			.off("error");
		this.dom.find(".image img")
			.on("load", () => {
				this.dom.find(".spinner img").hide();
				this.imageLoading = false;
				this.rotate(this.imgRotation);
				if(this.wControl.state.playing) {
					this.timer = setTimeout(this.play.bind(this), 2000);
				}
				else {
					clearTimeout(this.timer);
					this.timer = null;
				}
			})
			.on("error", () => {
				this.hasImageError = true;
				this.dom.find(".image img").attr("src", "img/notok.128.png");
				this.fixImgError();
			});
	}
	
	/**
	 * Handle image errors under Firefox
	 */
	fixImgError() {
		this.dom.find(".image img")
			.removeAttr("style")
			.removeAttr("hidden")
			.attr("style", "display: inherit !important");
		
		setTimeout(() => {
			if(this.dom.find(".image img").attr("style") !== "display: inherit !important") {
				this.fixImgError();
			}
		}, 100);
	}
	
	/**
	 * Rotate the picture 90° clock-wise
	 * @param newRotation {int} The new rotation value, in degrees (0 = North, 90 = East, 180 = South, 270 = West)
	 */
	rotate(newRotation) {
		this.imgRotation = newRotation;
		if(this.imgRotation >= 360) { this.imgRotation = 0; }
		this.dom.find(".image img").css("transform", "rotate("+this.imgRotation+"deg)");
		
		//Calculate image scale
		let scale = 1;
		let parentWidth = this.dom.find(".image").width();
		let parentHeight = this.dom.find(".image").height();
		let imgWidth = this.dom.find(".image img").width();
		let imgHeight = this.dom.find(".image img").height();
		
		if(imgHeight > parentHeight || imgWidth > parentWidth) {
			scale = Math.min(
				parentWidth / imgWidth,
				parentHeight / imgHeight
			);
		}
		
		this.dom.find(".image img").css("transform", "rotate("+this.imgRotation+"deg) scale("+scale+")");
	}
	
	/**
	 * Opens and zoom in JOSM on current picture area
	 */
	editJOSM() {
		let circle = L.circle(this.pictures[this.currentPicId].coordinates, { radius: 50 }).addTo(this.wLocation.map); //Added to map because we can use getBounds if not on map
		let bbox = circle.getBounds();
		this.wLocation.map.removeLayer(circle);
		$.ajax("http://127.0.0.1:8111/load_and_zoom?left="+bbox.getWest()+"&right="+bbox.getEast()+"&top="+bbox.getNorth()+"&bottom="+bbox.getSouth());
	}
	
	/**
	 * Opens ID editor on current picture area
	 */
	editId() {
		window.open(
			"https://www.openstreetmap.org/edit#map=19/"
				+this.pictures[this.currentPicId].coordinates.lat
				+"/"
				+this.pictures[this.currentPicId].coordinates.lng,
			"_blank"
		).focus();
	}
	
	on(event, handler) {
		return this.dom.on(event, handler);
	}
}

module.exports = Player;
