/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let $ = require("jquery");

/**
 * The description view shows in a DIV a short description of a given picture.
 */
class Description {
//CONSTRUCTOR
	constructor(divId) {
		this.dom = $("#"+divId);
		this.picture = null;
		
		//Create DIV structure
		this.dom.empty();
		this.dom.addClass("p4c-description");
		this.dom.append("<div class=\"content\"></div>");
		this.dom.find(".content").append(
			"<span class=\"title\">"+
				"<span class=\"provider\"></span>"+
				" - "+
				"<span class=\"date\"></span>"+
			"</span>"+
			"<span class=\"info\">"+
			"</span>"
		);
	}

//MODIFIERS
	/**
	 * Change the current picture
	 */
	set(p) {
		this.picture = p;
		
		//Update DOM
		this.dom.find("span.provider").text(this.picture.provider);
		
		let date = new Date(this.picture.date);
		this.dom.find("span.date").text(date.toLocaleDateString());
		
		//Optional attributes
		let info = "";
		if(this.picture.author) {
			info += "By "+this.picture.author;
		}
		
		if(this.picture.license) {
			info += (info.length > 0) ? ", u" : "U";
			info += "nder \""+this.picture.license+"\" license";
		}
		
		if(this.picture.detailsUrl) {
			if(info.length > 0) { info += " "; }
			info += "<a href=\""+this.picture.detailsUrl+"\" target=\"blank\">(Details)</a>";
		}
		
		this.dom.find("span.info").html(info);
	}
}

module.exports = Description;
