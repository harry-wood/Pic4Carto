/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let $ = require("jquery");
let Mousetrap = require("mousetrap");

/**
 * The control widget is a set of buttons, controlling the playing of images flow.
 * Each button sends events which can be listened.
 * It also listens to keyboard in order to launch appropriate events when shortcut is used.
 */
class Control {
//CONSTRUCTOR
	constructor(divId) {
		this.dom = $("#"+divId);
		
		//Init some buttons states
		this.state = {
			playing: false
		};
		
		//Create DIV structure
		this.dom.empty();
		this.dom.addClass("p4c-control");
		this.addButton("prev", { shortcut: "left" });
		this.addButton("play", { shortcut: "space" });
		this.addButton("next", { shortcut: "right" });
		this.addButton("edit", { shortcut: "e i", editor: "iD" });
		this.addButton("edit", { shortcut: "e j", editor: "JOSM" });
		
		//Events not linked to a button
		Mousetrap.bind("r", () => { this.dom.trigger("rotate"); });
	}

//OTHER METHODS
	/**
	 * Create the given button in DOM
	 * @param name The button name
	 * @param options Options for certain buttons (for example, editor for edit button)
	 */
	addButton(name, options) {
		let event = () => {};
		
		if(name == "play") {
			this.dom.append("<div class=\"btn-container\"><button class=\"btn-play\"type=\"button\" title=\""+name+((options.shortcut) ? " ("+options.shortcut+")" : "")+"\"><img src=\"img/"+((this.state.playing) ? "pause" : "play")+".64.png\" alt=\"play\" /></button></div>");
			
			//Event
			event = () => {
				this.state.playing = !this.state.playing;
				this.dom.find("button.btn-play img")
					.attr("src", "img/"+((this.state.playing) ? "pause" : "play")+".64.png")
					.attr("alt", (this.state.playing) ? "pause" : "play");
				this.dom.trigger((!this.state.playing) ? "pause" : "play");
			};
			
			this.dom.find("button.btn-play").click(event);
			if(options.shortcut) {
				Mousetrap.bind(options.shortcut, event);
			}
		}
		else if(name == "edit") {
			let fullname = name + "-" + options.editor.toLowerCase();
			this.dom.append("<div class=\"btn-container\"><button class=\"btn-edit btn-"+fullname+"\"type=\"button\" title=\""+name+((options.shortcut) ? " ("+options.shortcut+")" : "")+"\"><img src=\"img/edit.64.png\" alt=\"Edit\" /><span>"+options.editor+"</span></button></div>");
			
			//Event
			event = () => {
				this.dom.trigger(fullname);
				this.stopPlaying();
			};
			
			this.dom.find("button.btn-"+fullname).click(event);
		}
		else {
			this.dom.append("<div class=\"btn-container\"><button class=\"btn-"+name+"\"type=\"button\" title=\""+name+((options.shortcut) ? " ("+options.shortcut+")" : "")+"\"><img src=\"img/"+name+".64.png\" alt=\""+name+"\" /></button></div>");
			
			//Event
			event = () => {
				this.dom.trigger(name);
				this.stopPlaying();
			};
			
			this.dom.find("button.btn-"+name).click(event);
		}
		
		if(options.shortcut) {
			Mousetrap.bind(options.shortcut, event);
		}
	}
	
	/**
	 * Call this to set play button to pause state
	 */
	stopPlaying() {
		this.state.playing = false;
		this.dom.find("button.btn-play img")
			.attr("src", "img/play.64.png")
			.attr("alt", "play");
	}
	
	/**
	 * Event handler
	 * @param event The event to listen
	 * @param callback The callback function
	 */
	on(event, callback) {
		return this.dom.on(event, callback);
	}
	
	/**
	 * Disable event handler
	 * @param event The event to listen
	 * @param callback The callback function
	 */
	off(event, callback) {
		return this.dom.off(event, callback);
	}
}

module.exports = Control;
