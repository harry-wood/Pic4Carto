/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let L = require("leaflet");
L.Marker = require("./MarkerRotate");

let EventLauncher = require("../ctrl/EventLauncher");

/**
 * The location view is a map showing a position and optionnaly a direction of a photo shot.
 */
class Location extends EventLauncher {
//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param divId The DIV ID
	 * @param cell The current cell
	 */
	constructor(divId, cell) {
		super();
		
		this.cell = cell;
		
		//Map init
		this.map = L.map(divId, { scrollWheelZoom: "center", zoomControl: false }).fitBounds(this.cell.bounds);
		this.map.setMaxBounds(this.cell.bounds.pad(0.1));
		L.DomUtil.addClass(L.DomUtil.get(divId), "p4c-location");
		
		var http = window.location && window.location.protocol == 'https:' ? 'https:' : 'http:';
		L.tileLayer(http+'//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			name: "OpenStreetMap",
			attribution: "Tiles <a href=\"http://openstreetmap.org/\">OSM</a>",
			maxZoom: 19
		}).addTo(this.map);
		
		L.rectangle(this.cell.bounds, { fillOpacity: 0.1 }).addTo(this.map);
		
		//Marker icons
		this.markerIcons = {
			selectedDirected: L.icon({
				iconUrl: 'img/marker_directed_opaque.png',
				iconSize: [45.3, 43.3], //Original 68,65
				iconAnchor: [22.6, 32.6] //Original 34,49
			}),
			selectedUndirected: L.icon({
				iconUrl: 'img/marker_undirected_opaque.png',
				iconSize: [21.3, 21.3] //Original 32,32
			}),
			unselectedDirected: L.icon({
				iconUrl: 'img/marker_directed_transparent.png',
				iconSize: [22.6, 21.6], //Original 68,65
				iconAnchor: [11.3, 16.3] //Original 34,49
			}),
			unselectedUndirected: L.icon({
				iconUrl: 'img/marker_undirected_transparent.png',
				iconSize: [10.6, 10.6] //Original 32,32
			})
		};
		
		//Create markers
		this.markers = [];
		this.currentPicture = -1;
		
		for(let pId = 0; pId < this.cell.pictures.length; pId++) {
			let p = this.cell.pictures[pId];
			let myPid = pId;
			this.markers.push(new L.Marker(p.coordinates, { iconAngle: (this.isValidDirection(pId)) ? p.direction : 0, icon: (this.isValidDirection(pId)) ? this.markerIcons.unselectedDirected : this.markerIcons.unselectedUndirected }));
			this.markers[pId].addTo(this.map);
			this.markers[pId].on("click", () => { this.fire("click", { pictureId: myPid }); });
		}
	}

//MODIFIERS
	/**
	 * Edits the currently shown picture
	 * @param id The picture ID
	 */
	setCurrentPicture(id) {
		if(this.currentPicture >= 0) {
			this.markers[this.currentPicture]
				.setIcon(
					(this.isValidDirection(this.currentPicture)) ? this.markerIcons.unselectedDirected : this.markerIcons.unselectedUndirected
				)
				.setZIndexOffset(0);
		}
		this.markers[id]
			.setIcon(
				(this.isValidDirection(id)) ? this.markerIcons.selectedDirected : this.markerIcons.selectedUndirected
			)
			.setZIndexOffset(1000);
		this.map.setView(this.cell.pictures[id].coordinates, 19);
		this.currentPicture = id;
	}

//OTHER METHODS
	/**
	 * Is the direction of the given picture valid ?
	 * @param pid The picture ID
	 * @return True if this picture has a valid direction value
	 */
	isValidDirection(pid) {
		return this.cell.pictures[pid].direction !== undefined && typeof this.cell.pictures[pid].direction === "number" && this.cell.pictures[pid].direction >= 0 && this.cell.pictures[pid].direction < 360;
	}
}

module.exports = Location;
