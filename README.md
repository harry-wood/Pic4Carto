Pic4Carto
=========

![Pic4Carto logo](./src/img/logo_full.512.png)


Read-me
-------

Pic4Carto is an efficient street pictures viewer made for easier [OpenStreetMap](http://openstreetmap.org) editing. It allows to review fastly all recent street pictures in a given area, making possible to find small urban objects like fire hydrants, benches, street lamps, post boxes... and add them in OpenStreetMap editors. Pic4Carto is written in JavaScript (ES6).

A live demo is available at: http://projets.pavie.info/pic4carto/

### Available pictures providers
* [Flickr](https://www.flickr.com/) - Various open licenses
* [Mapillary](https://www.mapillary.com/) - [Creative Commons Attribution-ShareAlike 4.0](https://www.mapillary.com/legal)
* [Wikimedia Commons](http://commons.wikimedia.org/) - Various open licenses

### Shortcuts
Some shortcuts are available when watching a sequence of pictures:
* Left key: go to previous picture
* Right key: go to next picture
* Space bar: play/pause
* E then I: edit with iD editor
* E then J: edit with JOSM
* R: rotate image 90° clockwise

Build
-----

### Dependencies
* npm
* sed

### Compiling

If you want to build Pic4Carto by yourself, you can do the following (but it's not necessary for install, as a build is already available in [Tags page](https://framagit.org/PanierAvide/Pic4Carto/tags)):
```
npm install
make
```
When this is done, Pic4Carto is ready in **dist/** folder.


Installation
------------

If you want to install your own Pic4Carto instance, just upload the content of the **dist/** folder after build (or of **dist.zip**) in your own FTP or web server. That's all.


License
-------

Copyright 2016 Adrien PAVIE

See LICENSE for complete AGPL3 license.

Pic4Carto is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pic4Carto is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Pic4Carto. If not, see <http://www.gnu.org/licenses/>.
