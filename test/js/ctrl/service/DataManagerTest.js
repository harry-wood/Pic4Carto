/**
 * Test script for DataManager
 */

let L = require("leaflet");
let QUnit = require("qunitjs");
let DataManager = require("../../../../src/js/ctrl/service/DataManager");

QUnit.module("Ctrl > Service > DataManager");

/*
 * OTHER METHODS
 */

//ask
QUnit.test("ask fresh over not empty area", function(assert) {
	let bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	let dm1 = new DataManager();
	let done = assert.async();
	
// 	dm1.on("fetcherdone", f => { console.log("Fetcher "+f+" done"); });
// 	dm1.on("fetcherfailed", f => { console.log("Fetcher "+f+" failed"); });
	
	dm1.on("dataready", cells => {
// 		console.log("Cells: "+cells.length);
		assert.ok(cells.length > 0);
		let noPicturesCell = 0;
		
		for(let i=0; i < cells.length; i++) {
			let cell = cells[i];
			if(cell.pictures.length == 0) { noPicturesCell++; }
			assert.ok(cell.creationDate >= Date.now() - 6 * 60 * 60 * 1000);
		}
		assert.ok(noPicturesCell < cells.length / 4);
		
		setTimeout(() => { done(); }, 1000);
	});
	
	dm1.ask(bbox, true);
});

QUnit.test("ask cached over not empty area", function(assert) {
	let bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	let dm1 = new DataManager();
	let done = assert.async();
	
	dm1.on("dataready", cells => {
		assert.ok(cells.length > 0);
		
		//Now, ask using possibly cached data
// 		console.log("asking second time");
		dm1.off("dataready");
		dm1.on("dataready", cells => {
// 			console.log("Cells: "+cells.length);
			assert.ok(cells.length > 0);
			let noPicturesCell = 0;
			
			for(let i=0; i < cells.length; i++) {
				let cell = cells[i];
				if(cell.pictures.length == 0) { noPicturesCell++; }
				assert.ok(cell.creationDate >= Date.now() - 6 * 60 * 60 * 1000);
			}
			assert.ok(noPicturesCell < cells.length / 4);
			
			setTimeout(() => { done(); }, 1000);
		});
		
		dm1.ask(bbox);
	});
	
// 	console.log("asking first time");
	dm1.ask(bbox, true);
});

QUnit.test("ask fresh over empty area", function(assert) {
	let bbox = L.latLngBounds(L.latLng(35.75, -44.78), L.latLng(35.76, -44.77));
	let dm1 = new DataManager();
	let done = assert.async();
	
// 	dm1.on("fetcherdone", f => { console.log("Fetcher "+f+" done"); });
// 	dm1.on("fetcherfailed", f => { console.log("Fetcher "+f+" failed"); });
	
	dm1.on("dataready", cells => {
// 		console.log("Cells: "+cells.length);
		assert.ok(cells.length > 0);
		
		for(let i=0; i < cells.length; i++) {
			let cell = cells[i];
			assert.equal(cell.pictures.length, 0);
			assert.ok(cell.creationDate >= Date.now() - 6 * 60 * 60 * 1000);
		}
		
		setTimeout(() => { done(); }, 1000);
	});
	
	dm1.ask(bbox, true);
});

//askStatistics
QUnit.test("askStatistics fresh over not empty area", function(assert) {
	let bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	let dm1 = new DataManager();
	let done = assert.async();
	let nbCellsReady = 0;
	
	dm1.on("statscellready", (d) => {
		assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(d.bbox));
		assert.ok(!isNaN(d.stats.amount));
		assert.ok(d.stats.approxAmount !== undefined);
		if(d.stats.last) {
			assert.ok(d.stats.last >= Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
		}
		else {
			assert.equal(d.stats.amount, 0);
		}
		
		nbCellsReady++;
	});
	dm1.on("statsready", stats => {
// 		console.log(stats);
		assert.ok(Object.keys(stats).length > 0);
		assert.equal(Object.keys(stats).length, nbCellsReady);
		
		for(let s in stats) {
			let stat = stats[s];
			assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(s));
			assert.ok(!isNaN(stat.amount));
			assert.ok(stat.approxAmount !== undefined);
			if(stat.last) {
				assert.ok(stat.last >= Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
			}
			else {
				assert.equal(stat.amount, 0);
			}
		}
		
		setTimeout(() => { done(); }, 1000);
	});
	
	dm1.askStatistics(bbox, true);
});

QUnit.test("askStatistics cached over not empty area", function(assert) {
	let bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	let dm1 = new DataManager();
	let done = assert.async();
	let nbCellsReady = 0;
	
	dm1.on("statsready", stats1 => {
		assert.ok(Object.keys(stats1).length > 0);
		
		dm1.on("statscellready", (d) => {
			assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(d.bbox));
			assert.ok(!isNaN(d.stats.amount));
			assert.ok(d.stats.approxAmount !== undefined);
			if(d.stats.last) {
				assert.ok(d.stats.last >= Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
			}
			else {
				assert.equal(d.stats.amount, 0);
			}
			
			nbCellsReady++;
		});
		
		dm1.off("statsready");
		dm1.on("statsready", stats => {
			assert.equal(Object.keys(stats).length, nbCellsReady);
			
			for(let s in stats) {
				let stat = stats[s];
				assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(s));
				assert.ok(!isNaN(stat.amount));
				assert.ok(stat.approxAmount !== undefined);
				if(stat.last) {
					assert.ok(stat.last >= Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
				}
				else {
					assert.equal(stat.amount, 0);
				}
			}
			
			setTimeout(() => { done(); }, 1000);
		});
		
		dm1.askStatistics(bbox, false);
	});
	
	dm1.askStatistics(bbox, true);
});

QUnit.test("askStatistics fresh over empty area", function(assert) {
	let bbox = L.latLngBounds(L.latLng(35.75, -44.78), L.latLng(35.76, -44.77));
	let dm1 = new DataManager();
	let done = assert.async();
	let nbCellsReady = 0;
	
	dm1.on("statscellready", (d) => {
		assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(d.bbox));
		assert.ok(!isNaN(d.stats.amount));
		assert.ok(d.stats.approxAmount !== undefined);
		if(d.stats.last) {
			assert.ok(d.stats.last >= Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
		}
		else {
			assert.equal(d.stats.amount, 0);
		}
		
		nbCellsReady++;
	});
	dm1.on("statsready", stats => {
		assert.ok(Object.keys(stats).length > 0);
		assert.equal(Object.keys(stats).length, nbCellsReady);
		
		for(let s in stats) {
			let stat = stats[s];
			assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(s));
			assert.ok(!isNaN(stat.amount));
			assert.equal(stat.amount, 0);
			assert.notOk(stat.approxAmount);
		}
		
		setTimeout(() => { done(); }, 1000);
	});
	
	dm1.askStatistics(bbox, true);
});
