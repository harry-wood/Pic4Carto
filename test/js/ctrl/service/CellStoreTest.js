/**
 * Test script for CellStore
 */

let QUnit = require("qunitjs");
let L = require("leaflet");
let Cell = require("../../../../src/js/model/Cell");
let CellStore = require("../../../../src/js/ctrl/service/CellStore");
let Picture = require("../../../../src/js/model/Picture");

QUnit.module("Ctrl > Service > CellStore");


/*
 * ACCESSORS
 */

//get
QUnit.test("get with valid ID", function(assert) {
	let cs1 = new CellStore();
	let c1 = new Cell(L.latLngBounds(L.latLng(1,1), L.latLng(1.1, 1.1)));
	c1.addPicture(new Picture("http://test.net/img1.jpg", 1, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details1.html", 175));
	c1.addPicture(new Picture("http://test.net/img2.jpg", 2, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details2.html", 170));
	
	cs1.update(c1);
	let result = cs1.get(c1.getId());
	
	assert.ok(result !== null);
	assert.ok(result.bounds.equals(L.latLngBounds(L.latLng(1,1), L.latLng(1.1, 1.1))));
	assert.equal(result.creationDate, c1.creationDate);
	assert.equal(result.pictures.length, 2);
	assert.equal(result.pictures[0].pictureUrl, "http://test.net/img1.jpg");
	assert.equal(result.pictures[1].pictureUrl, "http://test.net/img2.jpg");
});

QUnit.test("get with ID of not known cell", function(assert) {
	let cs1 = new CellStore();
	let c1 = new Cell(L.latLngBounds(L.latLng(1,1), L.latLng(1.1, 1.1)));
	c1.addPicture(new Picture("http://test.net/img1.jpg", 1, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details1.html", 175));
	c1.addPicture(new Picture("http://test.net/img2.jpg", 2, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details2.html", 170));
	
	cs1.update(c1);
	let result = cs1.get("P4C-Cell#10.1,10.1,10.2,10.2");
	
	assert.equal(result, null);
});

//getStatistics
QUnit.test("getStatistics with valid ID", function(assert) {
	let cs1 = new CellStore();
	let st1 = { last: 123456789, amount: 20, approxAmount: true };
	
	cs1.updateStatistics("12.3,4.5,12.4,4.6", st1);
	let result = cs1.getStatistics("12.3,4.5,12.4,4.6");
	
	assert.ok(result !== null);
	assert.equal(result.last, 123456789);
	assert.equal(result.amount, 20);
	assert.ok(result.approxAmount);
});

QUnit.test("getStatistics with ID of not known cell", function(assert) {
	let cs1 = new CellStore();
	let st1 = { last: 123456789, amount: 20, approxAmount: true };
	
	cs1.updateStatistics("12.3,4.5,12.4,4.6", st1);
	let result = cs1.get("P4C-CellStat#10.1,10.1,10.2,10.2");
	
	assert.equal(result, null);
});


/*
 * MODIFIERS
 */

//update
QUnit.test("update", function(assert) {
	let cs1 = new CellStore();
	let c1 = new Cell(L.latLngBounds(L.latLng(1,1), L.latLng(1.1, 1.1)));
	c1.addPicture(new Picture("http://test.net/img1.jpg", 1, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details1.html", 175));
	c1.addPicture(new Picture("http://test.net/img2.jpg", 2, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details2.html", 170));
	
	cs1.update(c1);
	let result = cs1.get(c1.getId());
	
	assert.ok(result !== null);
	assert.ok(result.bounds.equals(L.latLngBounds(L.latLng(1,1), L.latLng(1.1, 1.1))));
	assert.equal(result.creationDate, c1.creationDate);
	assert.equal(result.pictures.length, 2);
	assert.equal(result.pictures[0].pictureUrl, "http://test.net/img1.jpg");
	assert.equal(result.pictures[1].pictureUrl, "http://test.net/img2.jpg");
	
	//Overwrite
	let c1new = new Cell(L.latLngBounds(L.latLng(1,1), L.latLng(1.1, 1.1)));
	c1new.addPicture(new Picture("http://test.net/img1_1.jpg", 1, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details1.html", 175));
	c1new.addPicture(new Picture("http://test.net/img2_1.jpg", 2, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details2.html", 170));
	
	cs1.update(c1new);
	let resultnew = cs1.get(c1.getId());
	
	assert.ok(resultnew !== null);
	assert.ok(resultnew.bounds.equals(L.latLngBounds(L.latLng(1,1), L.latLng(1.1, 1.1))));
	assert.equal(resultnew.creationDate, c1new.creationDate);
	assert.equal(resultnew.pictures.length, 2);
	assert.equal(resultnew.pictures[0].pictureUrl, "http://test.net/img1_1.jpg");
	assert.equal(resultnew.pictures[1].pictureUrl, "http://test.net/img2_1.jpg");
});

//updateStatistics
QUnit.test("updateStatistics", function(assert) {
	let cs1 = new CellStore();
	let st1 = { last: 123456789, amount: 20, approxAmount: true };
	
	cs1.updateStatistics("12.3,4.5,12.4,4.6", st1);
	let result = cs1.getStatistics("12.3,4.5,12.4,4.6");
	
	assert.ok(result !== null);
	assert.equal(result.last, 123456789);
	assert.equal(result.amount, 20);
	assert.ok(result.approxAmount);
	
	//Overwrite
	let st1new = { last: 123456, amount: 42, approxAmount: false };
	
	cs1.updateStatistics("12.3,4.5,12.4,4.6", st1new);
	let resultnew = cs1.getStatistics("12.3,4.5,12.4,4.6");
	
	assert.ok(resultnew !== null);
	assert.equal(resultnew.last, 123456);
	assert.equal(resultnew.amount, 42);
	assert.notOk(resultnew.approxAmount);
});
