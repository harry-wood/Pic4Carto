/**
 * Test script for GridExporter
 */

let QUnit = require("qunitjs");
let GridFactory = require("../../../../src/js/model/GridFactory");
let GridExporter = require("../../../../src/js/ctrl/service/GridExporter");

QUnit.module("Ctrl > Service > GridExporter");

/*
 * OTHER METHODS
 */

//ExportAsGeoJSON
QUnit.test("ExportAsGeoJSON", function(assert) {
	let cells = GridFactory.make(L.latLngBounds(L.latLng(-1.069,-1.059), L.latLng(-1.049, -1.049)));
	GridExporter.exportAsGeoJSON(cells);
	assert.expect(0);
});

//ExportAsOSMXML
QUnit.test("ExportAsOSMXML", function(assert) {
	let cells = GridFactory.make(L.latLngBounds(L.latLng(-1.069,-1.059), L.latLng(-1.049, -1.049)));
	GridExporter.exportAsOSMXML(cells);
	assert.expect(0);
});
