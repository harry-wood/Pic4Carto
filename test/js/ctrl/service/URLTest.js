/*
 * Test script for ctrl/service/URL.js
 */

let QUnit = require("qunitjs");
let URL = require("../../../../src/js/ctrl/service/URL");

QUnit.module("Ctrl > Service > URL");

/*
 * CONSTRUCTOR
 */
QUnit.test("Constructor with valid parameters", function(assert) {
	let geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.ok(u instanceof URL);
});


/*
 * ACCESSORS
 */

//getParameter
QUnit.test("getParameter with valid value", function(assert) {
	let geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.equal(u.getParameter("bbox"), "42.12,12.84,-7.23,-12.25");
});
QUnit.test("getParameter with invalid value", function(assert) {
	let geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.throws(
		function() { u.getParameter("missing") },
		new Error("ctrl.service.URL.invalidParameter")
	);
});
QUnit.test("getParameter with no query part from readURL", function(assert) {
	let geturl = function() { return "http://pic4carto.net/"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.equal(u.getParameter("bbox"), null);
});
QUnit.test("getParameter with a hash part and no query part from readURL", function(assert) {
	let geturl = function() { return "http://pic4carto.net/test.html#hashpart"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.equal(u.getParameter("bbox"), null);
});


/*
 * MODIFIERS
 */

//setParameter
QUnit.test("setParameter with valid value", function(assert) {
	let done = assert.async();
	let geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25#hashpart"; };
	let seturl = function(url) { assert.equal(url, "http://pic4carto.net/?bbox=1,2,3,4#hashpart"); done(); };
	let u = new URL(geturl, seturl);
	
	u.setParameter("bbox", "1,2,3,4");
	u.write();
});
QUnit.test("setParameter with invalid value", function(assert) {
	let geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25#hashpart"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.throws(
		function() { u.setParameter("missing", "nope") },
		new Error("ctrl.service.URL.invalidParameter")
	);
});
