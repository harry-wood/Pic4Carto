/*
 * Test script for ctrl/fetchers/Mapillary.js
 */

let QUnit = require("qunitjs");
let L = require("leaflet");
let Mapillary = require("../../../../src/js/ctrl/fetchers/Mapillary");
const CLIENT_ID = "eDhQTTdnRmZJNXdYZWUwRDQxc1NwdzoyMTgzMTUzNTY5YjI3OWZh";

QUnit.module("Ctrl > Fetchers > Mapillary");

/*
 * CONSTRUCTOR
 */
QUnit.test("Constructor with valid parameters", function(assert) {
	let m1 = new Mapillary(CLIENT_ID);
	
	//Check attributes
	assert.equal(m1.clientId, CLIENT_ID);
	assert.equal(m1.name, "Mapillary");
});

QUnit.test("Constructor with missing parameters", function(assert) {
	assert.throws(
		function() {
			let m1 = new Mapillary();
		},
		new Error("ctrl.fetchers.mapillary.invalidclientid")
	);
});


/*
 * ACCESSORS
 */

//get
QUnit.test("Get with valid parameters", function(assert) {
	let m1 = new Mapillary(CLIENT_ID);
	let bbox = L.latLngBounds(L.latLng(48.12421, -1.72100), L.latLng(48.12961, -1.70733));
	
	let done = assert.async();
	
	m1.on("done", pictures => {
		assert.ok(pictures.length > 0);
		
		let pic1 = pictures[1];
		assert.ok(pic1.author.length > 0);
		assert.equal(pic1.license, "CC By-SA 4.0");
		assert.ok(pic1.date > 0);
		assert.ok(pic1.pictureUrl.startsWith("https://d1cuyjsrcm0gby.cloudfront.net/"));
		assert.ok(pic1.detailsUrl.startsWith("https://www.mapillary.com/app/?pKey="));
		assert.equal(pic1.provider, "Mapillary");
		assert.ok(bbox.contains(pic1.coordinates));
		assert.equal(typeof pic1.direction, "number");
		done();
	});
	
	m1.on("failed", e => {
		console.log(e);
		assert.ok(false);
		done();
	});
	
	m1.get(bbox);
});

QUnit.test("Get with valid parameters on empty area", function(assert) {
	let m1 = new Mapillary(CLIENT_ID);
	let bbox = L.latLngBounds(L.latLng(45.30004812466808, -24.53090028975572), L.latLng(46.09329426310072, -22.752314896985553));
	
	let done = assert.async();

	m1.on("done", pictures => {
		assert.equal(pictures.length, 0);
		done();
	});
	
	m1.on("failed", e => {
		console.log(e);
		assert.ok(false);
		done();
	});
	
	m1.get(bbox);
});


//getLight
QUnit.test("GetLight with valid parameters", function(assert) {
	let m1 = new Mapillary(CLIENT_ID);
	let bbox = L.latLngBounds(L.latLng(48.12421, -1.72100), L.latLng(48.12961, -1.70733));
	
	let done = assert.async();
	
	m1.on("donelight", stats => {
		assert.ok(stats.amount.length > 0);
		assert.ok(/^[e>][0-9]+$/.test(stats.amount));
		assert.ok(stats.last > Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
		assert.equal(stats.bbox, bbox.toBBoxString());
		done();
	});
	
	m1.on("failed", e => {
		console.log(e);
		assert.ok(false);
		done();
	});
	
	m1.getLight(bbox);
});
