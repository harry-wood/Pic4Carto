/*
 * Test script for ctrl/fetchers/Flickr.js
 */

let QUnit = require("qunitjs");
let L = require("leaflet");
let Flickr = require("../../../../src/js/ctrl/fetchers/Flickr");
const API_KEY = "b8c50865a2c22d93d3e673de8bd50178";

QUnit.module("Ctrl > Fetchers > Flickr");

/*
 * CONSTRUCTOR
 */
QUnit.test("Constructor with valid parameters", function(assert) {
	let m1 = new Flickr(API_KEY);
	
	//Check attributes
	assert.equal(m1.apiKey, API_KEY);
	assert.equal(m1.name, "Flickr");
});

QUnit.test("Constructor with missing parameters", function(assert) {
	assert.throws(
		function() {
			let m1 = new Flickr();
		},
		new Error("ctrl.fetchers.flickr.invalidapikey")
	);
});


/*
 * ACCESSORS
 */
QUnit.test("Get with valid parameters", function(assert) {
	let m1 = new Flickr(API_KEY);
	let bbox = L.latLngBounds(L.latLng(48.85,2.2899999999999996), L.latLng(48.86,2.2999999999999994));
	
	let done = assert.async();
	
	m1.on("done", pictures => {
		assert.ok(pictures.length > 0);

		for(let i=0; i < pictures.length && i < 20; i++) {
			let pic = pictures[i];
			assert.ok(pic.author.length > 0);
			assert.ok(pic.date <= Date.now());
			assert.ok(pic.date >= Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
			assert.ok(pic.pictureUrl.startsWith("https://farm"));
			assert.ok(pic.detailsUrl.startsWith("https://www.flickr.com/photos/"));
			assert.equal(pic.provider, "Flickr");
			assert.ok(bbox.contains(pic.coordinates));
			assert.equal(pic.direction, null);
		}
		done();
	});
	
	m1.on("failed", e => {
		console.log(e);
		assert.ok(false);
		done();
	});
	
	m1.get(bbox);
});

QUnit.test("Get with valid parameters on empty area", function(assert) {
	let m1 = new Flickr(API_KEY);
	let bbox = L.latLngBounds(L.latLng(31.410000000000004,-42.43000000000001), L.latLng(31.420000000000005,-42.42000000000001));
	
	let done = assert.async();
	
	m1.on("done", pictures => {
		assert.equal(pictures.length, 0);
		done();
	});
	
	m1.on("failed", e => {
		console.log(e);
		assert.ok(false);
		done();
	});
	
	m1.get(bbox);
});

//getLight
QUnit.test("GetLight with valid parameters", function(assert) {
	let m1 = new Flickr(API_KEY);
	let bbox = L.latLngBounds(L.latLng(48.85,2.2899999999999996), L.latLng(48.86,2.2999999999999994));
	
	let done = assert.async();
	
	m1.on("donelight", stats => {
		assert.ok(stats.amount.length > 0);
		assert.ok(/^[e>][0-9]+$/.test(stats.amount));
		assert.ok(stats.last > Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
		assert.equal(stats.bbox, bbox.toBBoxString());
		done();
	});
	
	m1.on("failed", e => {
		console.log(e);
		assert.ok(false);
		done();
	});
	
	m1.getLight(bbox);
});
