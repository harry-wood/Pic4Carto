/**
 * Test script for EventLauncher
 */

let QUnit = require("qunitjs");
let EventLauncher = require("../../../src/js/ctrl/EventLauncher");

QUnit.module("Ctrl > EventLauncher");

class Subclass extends EventLauncher {
	constructor() {
		super();
		return true;
	}
}

/*
 * CONSTRUCTOR
 */
QUnit.test("Constructor which can't be instantiated", function(assert) {
	assert.throws(
		() => { let el1 = new EventLauncher(); },
		new TypeError("ctrl.eventlauncher.cantinstantiate")
	);
});

QUnit.test("Constructor of heriting class", function(assert) {
	assert.ok(new Subclass());
	
	let sc1 = new Subclass();
	assert.ok(sc1.fire);
	assert.ok(sc1.on);
	assert.ok(sc1.off);
});


/*
 * OTHER METHODS
 */

//fire
QUnit.test("fire with associated object", function(assert) {
	let sc1 = new Subclass();
	let done = assert.async();
	
	sc1.on("testfire", (o) => {
		assert.ok(o.defined);
		done();
	});
	
	sc1.fire("testfire", { defined: true });
});

//on
QUnit.test("on with multiple listeners", function(assert) {
	let sc1 = new Subclass();
	let done = assert.async(2);
	
	sc1.on("testfire", (o) => {
		assert.ok(o.defined);
		done();
	});
	
	sc1.on("testfire", (o) => {
		assert.ok(o.test2);
		done();
	});
	
	sc1.fire("testfire", { defined: true, test2: true });
});

//off
QUnit.test("off on one listener", function(assert) {
	let sc1 = new Subclass();
	let done = assert.async(3);
	
	let l1 = (o) => {
		assert.ok(o.defined);
		done();
	};
	let l2 = (o) => {
		assert.ok(o.test2);
		done();
	};
	
	sc1.on("testfire", l1);
	sc1.on("testfire", l2);
	
	sc1.fire("testfire", { defined: true, test2: true });
	
	setTimeout(
		() => {
			sc1.off("testfire", l1);
			sc1.fire("testfire", { defined: true, test2: true });
		},
		100
	);
});

QUnit.test("off on all listeners", function(assert) {
	let sc1 = new Subclass();
	let done = assert.async(1);
	
	let l1 = (o) => {
		assert.ok(false);
		done();
	};
	let l2 = (o) => {
		assert.ok(false);
		done();
	};
	
	sc1.on("testfire", l1);
	sc1.on("testfire", l2);
	
	setTimeout(
		() => {
			sc1.off("testfire");
			sc1.fire("testfire", { defined: true, test2: true });
			setTimeout(() => {
					assert.ok(true);
					done();
				},
				200
			);
		},
		100
	);
});
