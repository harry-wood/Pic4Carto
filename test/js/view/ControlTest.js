/**
 * Expected behaviour: control is shown, and clicking on buttons shows alerts
 */

let Control = require("../../../src/js/view/Control");

let c1 = new Control("control");

c1.on("play", () => { alert("Clicked on play button"); });
c1.on("pause", () => { alert("Clicked on pause button"); });
c1.on("prev", () => { alert("Clicked on previous button"); });
c1.on("next", () => { alert("Clicked on next button"); });
c1.on("edit-id", () => { alert("Clicked on iD editor button"); });
c1.on("edit-josm", () => { alert("Clicked on JOSM editor button"); });
c1.on("rotate", () => { alert("Clicked on rotate shortcut"); });
