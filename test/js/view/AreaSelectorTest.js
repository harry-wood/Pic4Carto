/*
 * Expected behaviour: show two maps (one per select mode), and shows alert when users selects area.
 */

let L = require("leaflet");
let AreaSelector = require("../../../src/js/view/AreaSelector");
let GridFactory = require("../../../src/js/model/GridFactory");

// let as1 = new AreaSelector("map-free", "free", 14, (bbox) => { alert("Map free: "+bbox.toBBoxString()); });
let as2 = new AreaSelector("map-grid", "grid", 13, (bbox) => { alert("Map grid: "+bbox.toBBoxString()); });
// let sync1 = () => { syncMaps(as1.map.getCenter(), as1.map.getZoom()); };
// let sync2 = () => { syncMaps(as2.map.getCenter(), as2.map.getZoom()); };

// function syncMaps(center, zoom) {
// 	as1.map.off("zoomend moveend", sync1);
// 	as2.map.off("zoomend moveend", sync2);
// 	as1.map.setView(center, zoom);
// 	as2.map.setView(center, zoom);
// 	as1.map.on("zoomend moveend", sync1);
// 	as2.map.on("zoomend moveend", sync2);
// }
// 
// as1.map.on("zoomend moveend", sync1);
// as2.map.on("zoomend moveend", sync2);

as2.map.on("moveend zoomend", () => {
	if(as2.map.getZoom() >= 13) {
		let bbox = as2.map.getBounds();
		let cells = GridFactory.make(bbox);
		as2.setCellData(cells[0], { amount: 42, approxAmount: false, last: 12345678910 });
	}
});
