/**
 * Expected behaviour: show three sources, first successes, second fails, third successes
 */

let Loading = require("../../../src/js/view/Loading");

let l1 = new Loading(
			"loading",
			{
				"mapillary": {
					"name": "Mapillary",
					"img": "img/mapillary.512.png"
				},
				"wikicommons": {
					"name": "Wikimedia Commons",
					"img": "img/wikicommons.512.png"
				},
				"flickr": {
					"name": "Flickr",
					"img": "img/flickr.512.png"
				}
			}
		);

l1.on("completed", () => { alert("Loading done !!"); });
l1.on("skip", () => { alert("User wants to skip loading"); });

setTimeout(() => { l1.done("mapillary"); }, 2000);
setTimeout(() => { l1.failed("wikicommons"); }, 6000);
setTimeout(() => { l1.done("flickr"); }, 10000);
