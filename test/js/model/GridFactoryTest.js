/**
 * Test script for GridFactory
 */

let QUnit = require("qunitjs");
let L = require("leaflet");
let GridFactory = require("../../../src/js/model/GridFactory");

QUnit.module("Model > GridFactory");

/*
 * OTHER FUNCTIONS
 */

//make
QUnit.test("make with exactly matching bbox", function(assert) {
	let result = GridFactory.make(L.latLngBounds(L.latLng(1,1), L.latLng(1.02, 1.03)));
	
	assert.equal(result.length, 24);
	assert.ok(result[0].bounds.equals(L.latLngBounds(L.latLng(1,1), L.latLng(1.005,1.005))));
	assert.ok(result[1].bounds.equals(L.latLngBounds(L.latLng(1.005,1), L.latLng(1.01,1.005))));
	assert.ok(result[2].bounds.equals(L.latLngBounds(L.latLng(1.01,1), L.latLng(1.015,1.005))));
	assert.ok(result[3].bounds.equals(L.latLngBounds(L.latLng(1.015,1), L.latLng(1.02,1.005))));
	
	assert.ok(result[4].bounds.equals(L.latLngBounds(L.latLng(1,1.005), L.latLng(1.005,1.01))));
	assert.ok(result[5].bounds.equals(L.latLngBounds(L.latLng(1.005,1.005), L.latLng(1.01,1.01))));
	assert.ok(result[6].bounds.equals(L.latLngBounds(L.latLng(1.01,1.005), L.latLng(1.015,1.01))));
	assert.ok(result[7].bounds.equals(L.latLngBounds(L.latLng(1.015,1.005), L.latLng(1.02,1.01))));
	
	assert.ok(result[23].bounds.equals(L.latLngBounds(L.latLng(1.015,1.025), L.latLng(1.02,1.03))));
});

QUnit.test("make with non-exactly matching bbox", function(assert) {
	let result = GridFactory.make(L.latLngBounds(L.latLng(1.011,1.012), L.latLng(1.019, 1.029)));
	
	// 1.01, 1.01, 1.02, 1.03
	assert.equal(result.length, 8);
	assert.ok(result[0].bounds.equals(L.latLngBounds(L.latLng(1.01,1.01), L.latLng(1.015,1.015))));
	assert.ok(result[1].bounds.equals(L.latLngBounds(L.latLng(1.015,1.01), L.latLng(1.02,1.015))));
	assert.ok(result[2].bounds.equals(L.latLngBounds(L.latLng(1.01,1.015), L.latLng(1.015,1.02))));
	assert.ok(result[3].bounds.equals(L.latLngBounds(L.latLng(1.015,1.015), L.latLng(1.02,1.02))));
	assert.ok(result[4].bounds.equals(L.latLngBounds(L.latLng(1.01,1.02), L.latLng(1.015,1.025))));
	assert.ok(result[5].bounds.equals(L.latLngBounds(L.latLng(1.015,1.02), L.latLng(1.02,1.025))));
	assert.ok(result[6].bounds.equals(L.latLngBounds(L.latLng(1.01,1.025), L.latLng(1.015,1.03))));
	assert.ok(result[7].bounds.equals(L.latLngBounds(L.latLng(1.015,1.025), L.latLng(1.02,1.03))));
});

QUnit.test("make with non-exactly matching negative bbox", function(assert) {
	let result = GridFactory.make(L.latLngBounds(L.latLng(-1.069,-1.059), L.latLng(-1.049, -1.049)));
	
	// -1.07, -1.06, -1.045, -1.045
	assert.equal(result.length, 15);
	assert.ok(result[0].bounds.equals(L.latLngBounds(L.latLng(-1.07, -1.06), L.latLng(-1.065,-1.055))));
	assert.ok(result[1].bounds.equals(L.latLngBounds(L.latLng(-1.065, -1.06), L.latLng(-1.060,-1.055))));
	assert.ok(result[2].bounds.equals(L.latLngBounds(L.latLng(-1.06, -1.06), L.latLng(-1.055,-1.055))));
	assert.ok(result[3].bounds.equals(L.latLngBounds(L.latLng(-1.055, -1.06), L.latLng(-1.05,-1.055))));
	assert.ok(result[4].bounds.equals(L.latLngBounds(L.latLng(-1.05, -1.06), L.latLng(-1.045,-1.055))));
	
	assert.ok(result[10].bounds.equals(L.latLngBounds(L.latLng(-1.07, -1.05), L.latLng(-1.065,-1.045))));
	assert.ok(result[11].bounds.equals(L.latLngBounds(L.latLng(-1.065, -1.05), L.latLng(-1.060,-1.045))));
	assert.ok(result[12].bounds.equals(L.latLngBounds(L.latLng(-1.06, -1.05), L.latLng(-1.055,-1.045))));
	assert.ok(result[13].bounds.equals(L.latLngBounds(L.latLng(-1.055, -1.05), L.latLng(-1.05,-1.045))));
	assert.ok(result[14].bounds.equals(L.latLngBounds(L.latLng(-1.05, -1.05), L.latLng(-1.045,-1.045))));
});

QUnit.test("make with non-exactly matching extremely negative bbox", function(assert) {
	let result = GridFactory.make(L.latLngBounds(L.latLng(-53.6291,-71.9333), L.latLng(-53.6123, -71.8990)));
	
	// -53.63, -71.935, -53.61, -71.895
	assert.equal(result.length, 32);
	assert.ok(result[0].bounds.equals(L.latLngBounds(L.latLng(-53.63, -71.935), L.latLng(-53.625, -71.93))));
	assert.ok(result[1].bounds.equals(L.latLngBounds(L.latLng(-53.625, -71.935), L.latLng(-53.62, -71.93))));
	assert.ok(result[2].bounds.equals(L.latLngBounds(L.latLng(-53.62, -71.935), L.latLng(-53.615, -71.93))));
	assert.ok(result[3].bounds.equals(L.latLngBounds(L.latLng(-53.615, -71.935), L.latLng(-53.61, -71.93))));
	
	assert.ok(result[28].bounds.equals(L.latLngBounds(L.latLng(-53.63, -71.9), L.latLng(-53.625,-71.895))));
	assert.ok(result[29].bounds.equals(L.latLngBounds(L.latLng(-53.625, -71.9), L.latLng(-53.62,-71.895))));
	assert.ok(result[30].bounds.equals(L.latLngBounds(L.latLng(-53.62, -71.9), L.latLng(-53.615, -71.895))));
	assert.ok(result[31].bounds.equals(L.latLngBounds(L.latLng(-53.615, -71.9), L.latLng(-53.61, -71.895))));
});
